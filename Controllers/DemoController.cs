﻿using Code_First_Approach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Code_First_Approach.Controllers
{
    public class DemoController : Controller
    {
        EmpDataContext objDataContext = new EmpDataContext();
        // GET: Demo  
        // GET: create
        [HttpGet]
        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult create(Employee objEmp)
        {
            objDataContext.employees.Add(objEmp);
            objDataContext.SaveChanges();
            return View();
        }


        public ActionResult EmpDetails()
        {

            return View(objDataContext.employees.ToList());
        }

       public ActionResult Details(string id)
        {
            int empId = Convert.ToInt32(id);
            var emp = objDataContext.employees.Find(empId);
            return View(emp);
        }

        public ActionResult Edit(string id)
        {
            int empId = Convert.ToInt32(id);
            var emp = objDataContext.employees.Find(empId);
            return View(emp);
        }

        [HttpPost]
        public ActionResult Edit(Employee objEmp)
        {
            var data = objDataContext.employees.Find(objEmp.EmpId);
            if(data!=null)
            {
                data.Name = objEmp.Name;
                data.Address = objEmp.Address;
                data.Email = objEmp.Email;
                data.MobileNo = objEmp.MobileNo;
            }
            objDataContext.SaveChanges();
            return RedirectToAction("EmpDetails");
        }

        public ActionResult Delete(string id)
        {
            int empId = Convert.ToInt32(id);
            var emp = objDataContext.employees.Find(empId);
            return View(emp);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var data = objDataContext.employees.Find(id);
            if (data != null)
            {
                objDataContext.employees.Remove(data);
                objDataContext.SaveChanges();
            }
            return RedirectToAction("EmpDetails");
        }
    }
}