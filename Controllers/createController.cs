﻿using Code_First_Approach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Code_First_Approach.Controllers
{
    public class createController : Controller
    {
        // GET: create
        [HttpGet]
        public ActionResult create()
        {
            return View();
        }

        
        EmpDataContext objDataContext = new EmpDataContext();
        [HttpPost]
        public ActionResult create(Employee objEmp)
        {
            objDataContext.employees.Add(objEmp);
            objDataContext.SaveChanges();
            return View();
        }
    }
}
    